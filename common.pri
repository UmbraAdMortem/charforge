# The following keeps the generated files at least somewhat separate
# from the source files.
DESTDIR = bin
UI_DIR = uics
MOC_DIR = mocs
OBJECTS_DIR = objs

VERSION = $$system(git describe --tags)
