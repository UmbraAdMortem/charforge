TEMPLATE = subdirs

SUBDIRS = \
    charForge \
    uFrameWorkBase \
## unit test
    ut-uFrameWorkBase

# where to find the sub projects - give the folders
charForge.subdir = CharForge
uFrameWorkBase.subdir = UFrameWorkBase
## unit test
ut-uFrameWorkBase.subdir = UT-UFrameWorkBase

# what subproject depends on others
charForge.depends = uFrameWorkBase
## unit test
ut-uFrameWorkBase.depends = uFrameWorkBase
