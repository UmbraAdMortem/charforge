﻿#ifndef __PROPERTIES_H__
#define __PROPERTIES_H__

#include <gitInfo.h>
#ifndef VER_COMMIT
#define GIT_VERSION QString("%1.%2.%3.%4.%5").arg(VER_MAJOR).arg(VER_MINOR).arg(VER_PATCH).arg(0).arg(VER_COMMENT_COMB).remove("\"").remove("\\0")
#else
    #define GIT_VERSION QString("%1.%2.%3.%4.%5").arg(VER_MAJOR).arg(VER_MINOR).arg(VER_PATCH).arg(VER_COMMIT).arg(VER_COMMENT_COMB).remove("\"").remove("\\0")
#endif

#define PROP_COMPANYNAME      "UAM"
#define PROP_FILEDESCRIPTION  "Application to create character sheet for different pen and paper games."
#define PROP_PRODUCTNAME       "CharForge"

#endif  //__PROPERTIES_H__
