# Check if the config file exists
! include( ../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Generate gitInfo.h (versioning)
gitInfoTarget.target = gitInfo
gitInfoTarget.commands = echo "gitInfo.pl"; \
                    perl $$PWD/gitInfo.pl gitInfo.h
gitInfoTarget.depends = FORCE
QMAKE_EXTRA_TARGETS += gitInfoTarget
PRE_TARGETDEPS = gitInfo

HEADERS += \
    charForge/charForge.h \
    gitInfo.h \
    properties.h

SOURCES += \
        charForge/charForge.cpp \
        charForge/main.cpp

TRANSLATIONS += \
    languages/CharForge_en_GB.ts \
    languages/CharForge_de_AT.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#win32: {
#HEADERS += \
#    properties_win.h \
#    properties_win.rc
#}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../UFrameWorkBase/bin/ -lUFrameWorkBase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../UFrameWorkBase/bin/ -lUFrameWorkBased
else:unix: LIBS += -L$$OUT_PWD/../UFrameWorkBase/bin/ -lUFrameWorkBase

INCLUDEPATH += $$PWD/../UFrameWorkBase
DEPENDPATH += $$PWD/../UFrameWorkBase

DISTFILES += \
    ../CHANGELOG.md \
    ../README.md
