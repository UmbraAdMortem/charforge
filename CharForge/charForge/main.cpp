﻿#include "charForge/charForge.h"

int main(int argc, char *argv[])
{
    return CharForge::instance().exec(argc, &argv);
}
