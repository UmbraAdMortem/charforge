﻿#ifndef CHARFORGE_H
#define CHARFORGE_H

#include <worker/exports/uBaseMainWorker.h>

#include <properties.h>

class CharForge : public base::UBaseMainWorker
{
    Q_OBJECT
public:
    static CharForge& instance();

    CharForge(const CharForge&) = delete;
    CharForge& operator=(const CharForge) = delete;

private:
    CharForge();

    inline const QString organisationName() final { return PROP_COMPANYNAME; }
    inline const QString applicationName() final { return PROP_PRODUCTNAME; }
    inline const QString applicationVersion() final { return GIT_VERSION; }
    inline const QString applicationDescription() final { return QString(PROP_FILEDESCRIPTION); }
};

#endif // CHARFORGE_H
