﻿#include "UTestFrameWorkBase.h"

UTestFrameWorkBase* UTestFrameWorkBase::m_pInstance = nullptr;

UTestFrameWorkBase& UTestFrameWorkBase::instance()
{
    if(m_pInstance == nullptr)
        m_pInstance = new UTestFrameWorkBase();
    return *m_pInstance;
}

void UTestFrameWorkBase::deleteInstance()
{
    if(m_pInstance)
    {
        delete m_pInstance;
        m_pInstance = nullptr;
    }
}

UTestFrameWorkBase::UTestFrameWorkBase()
{

}
