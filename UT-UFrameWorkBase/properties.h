﻿#ifndef __PROPERTIES_H__
#define __PROPERTIES_H__

#ifndef GIT_VERSION
#include <gitInfo.h>
#ifndef VER_COMMIT
#define GIT_VERSION QString("%1.%2.%3.%4.%5").arg(VER_MAJOR).arg(VER_MINOR).arg(VER_PATCH).arg(0).arg(VER_COMMENT_COMB).remove("\"").remove("\\0")
#else
    #define GIT_VERSION QString("%1.%2.%3.%4.%5").arg(VER_MAJOR).arg(VER_MINOR).arg(VER_PATCH).arg(VER_COMMIT).arg(VER_COMMENT_COMB).remove("\"").remove("\\0")
#endif
#endif

#define PROP_COMPANYNAME      "UAM"
#define PROP_FILEDESCRIPTION  "Unit test of Base Library of UFrameWork"
#define PROP_PRODUCTNAME      "UT-UFrameWorkBase"

#endif  //__PROPERTIES_H__
