QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  \
    UTestFrameWorkBase.cpp \
    tst_ut_uFrameWorkBase.cpp

HEADERS += \
    UTestFrameWorkBase.h \
    properties.h \
    tst_ut_uFrameWorkBase.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../UFrameWorkBase/bin/ -lUFrameWorkBase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../UFrameWorkBase/bin/ -lUFrameWorkBased
else:unix: LIBS += -L$$OUT_PWD/../UFrameWorkBase/bin/ -lUFrameWorkBase

INCLUDEPATH += $$PWD/../UFrameWorkBase
DEPENDPATH += $$PWD/../UFrameWorkBase

VERSION = $$system(git describe --tags)
DEFINES += GIT_VERSION=\\\"$$system(git describe --tags)\\\"
