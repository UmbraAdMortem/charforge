﻿#ifndef UTESTFRAMEWORKBASE_H
#define UTESTFRAMEWORKBASE_H

#include <worker/exports/uBaseMainWorker.h>

class UTestFrameWorkBase : public base::UBaseMainWorker
{
    Q_OBJECT
public:
    static UTestFrameWorkBase& instance();
    static void deleteInstance();

    UTestFrameWorkBase(const UTestFrameWorkBase&) = delete;
    UTestFrameWorkBase& operator=(const UTestFrameWorkBase) = delete;

private:
    static UTestFrameWorkBase* m_pInstance;
    UTestFrameWorkBase();

    inline const QString organisationName() final { return PROP_COMPANYNAME; }
    inline const QString applicationName() final { return PROP_PRODUCTNAME; }
    inline const QString applicationVersion() final { return GIT_VERSION; }
    inline const QString applicationDescription() final { return QString(PROP_FILEDESCRIPTION); }
};

#endif // UTESTFRAMEWORKBASE_H
