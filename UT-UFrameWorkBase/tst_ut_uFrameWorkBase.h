﻿#ifndef TST_UT_UFRAMEWORKBASE_H
#define TST_UT_UFRAMEWORKBASE_H

#include <QtTest>

#include "UTestFrameWorkBase.h"

typedef  QMap<int, QRegularExpression> URegExMap;

class UT_UFrameWorkBase : public QObject
{
    Q_OBJECT
public:
    UT_UFrameWorkBase();
    ~UT_UFrameWorkBase() {}

private:
    QStringList& filterStringList(QStringList& qstrlData, const URegExMap& qmFilter);

    void tcCLInput_dataSetColumn();
    QByteArray tcCLInput_dataAutoChar();
    void tcCLInput_dataGenerator(const QByteArray& qbalValidChars,
                                 const QByteArrayList& qstrlUsed,
                                 const QStringList& qstrlHelpOutput,
                                 int iStrLength = 1,
                                 const QByteArray& qbaPrefix = QByteArray("-"),
                                 const QByteArray& qbaSuffix = QByteArray(""),
                                 const QByteArrayList& qbalParameter = QByteArrayList(),
                                 const QByteArray& qbaLetters = QByteArray());
    void tcCLInput_execApplication(const QByteArrayList& qbalCommandLineArgument, int iExpectedExitCode, FILE* pfOutput = nullptr);
    void tcCLInput();

    QByteArrayList m_qbalCheckedCLO;

private slots:
    // called before the first test case
    void initTestCase();
    // called before every test case
    void init();
    // called after every test case
    void cleanup();
    // called after the last test case
    void cleanupTestCase();

    // Test Cases
    void tc_CLInputImplShort_data();
    void tc_CLInputImplShort() { tcCLInput(); }
    void tc_CLInputAutoGenShort_data();
    void tc_CLInputAutoGenShort() { tcCLInput(); }
    void tc_CLInputImplLong_data();
    void tc_CLInputImplLong() { tcCLInput(); }
    void tc_CLInputAutoGenLong_data();
    void tc_CLInputAutoGenLong() { tcCLInput(); }
    void tc_CLInputImplShortM_config_data();
    void tc_CLInputImplShortM_config() { tcCLInput(); }
    void tc_CLInputAutoGenShortM_config_data();
    void tc_CLInputAutoGenShortM_config() { tcCLInput(); }
    void tc_CLInputImplLongM_config_data();
    void tc_CLInputImplLongM_config() { tcCLInput(); }
    void tc_CLInputAutoGenLongM_config_data();
    void tc_CLInputAutoGenLongM_config() { tcCLInput(); }
    void tc_CLInputSingle_data();
    void tc_CLInputSingle() { tcCLInput(); }
    //  TODO: tc_uLog
    //  TODO: tc_uConfiguration

};
#endif // TST_UT_UFRAMEWORKBASE_H
