﻿#include "tst_ut_uFrameWorkBase.h"

#include <QDebug>
#include <properties.h>

UT_UFrameWorkBase::UT_UFrameWorkBase()
{
    m_qbalCheckedCLO.clear();
}

QStringList& UT_UFrameWorkBase::filterStringList(QStringList& qstrlData, const URegExMap& qmFilter)
{
    int iPos = 0;
    QStringList qstrlTemp;
    for(QMap<int, QRegularExpression>::ConstIterator qmiFilter = qmFilter.constBegin();
        qmiFilter != qmFilter.constEnd();
        qmiFilter++)
    {
        qstrlTemp << qstrlData.mid(iPos, qmiFilter.key() - iPos);
        iPos = qmiFilter.key() + 1;
        if(qstrlData.size() > qmiFilter.key())
        {
            QRegularExpressionMatch match = qmiFilter.value().match(qstrlData.at(qmiFilter.key()));
            if(match.capturedTexts().size() > 1)
                qstrlTemp << match.capturedTexts().mid(1, -1);
        }
    }
    if(!qstrlTemp.isEmpty())
    {
        qstrlTemp << qstrlData.mid(iPos, -1);
        qstrlData = qstrlTemp;
    }
    return qstrlData;
}

void UT_UFrameWorkBase::tcCLInput_dataSetColumn()
{
    QTest::addColumn<QList<QByteArrayList>>("qlCommandLineArgument");
    QTest::addColumn<int>("iExpectedExitCode");
    QTest::addColumn<QByteArray>("qstrCommandLineOutputFile");
    QTest::addColumn<QStringList>("qstrlCommandLineOutput");
    QTest::addColumn<URegExMap>("qmFilter");
}

QByteArray UT_UFrameWorkBase::tcCLInput_dataAutoChar()
{
    QByteArray qbaTestChar;
    for(char cChar = '0'; cChar <= '9'; cChar++)
        qbaTestChar.append(cChar);
    for(char cChar = 'A'; cChar <= 'Z'; cChar++)
        qbaTestChar.append(cChar);
    for(char cChar = 'a'; cChar <= 'z'; cChar++)
        qbaTestChar.append(cChar);

    return qbaTestChar;
}

void UT_UFrameWorkBase::tcCLInput_dataGenerator(const QByteArray& qbaValidChars,
                                                const QByteArrayList& qstrlUsed,
                                                const QStringList& qstrlHelpOutput,
                                                int iStrLength,
                                                const QByteArray& qbaPrefix,
                                                const QByteArray& qbaSuffix,
                                                const QByteArrayList& qbalParameter,
                                                const QByteArray& qbaLetters)
{
    if(iStrLength == 0)
        return;
    for(int iChar = 0; iChar < qbaValidChars.size(); iChar++)
    {
        QByteArray qbaArg(qbaLetters);
        qbaArg.append(qbaValidChars.at(iChar));

        tcCLInput_dataGenerator(qbaValidChars, qstrlUsed, qstrlHelpOutput, iStrLength - 1, qbaPrefix, qbaSuffix, qbalParameter, qbaArg);

        if(qbaValidChars.at(iChar) == '-' || qstrlUsed.contains(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ")))
            continue;
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Unknown option: \"%1\"").arg(qbaArg.data())
                    << ""
                    << qstrlHelpOutput)
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }
}

void UT_UFrameWorkBase::tcCLInput_execApplication(const QByteArrayList& qbalCommandLineArgument, int iExpectedExitCode, FILE* pfOutput)
{
    QByteArrayList qbalArgs;
    qbalArgs << qbalCommandLineArgument;
    qbalArgs.prepend(QDir::currentPath().toUtf8());
    qbalArgs.removeAll("");

    int argc = qbalArgs.size();

    char** argv;
    argv = (char**)malloc(sizeof(char*) * argc);
    for(int iArg = 0; iArg < argc; iArg++)
    {
        argv[iArg] = (char*)malloc(sizeof (char) * qbalArgs.at(iArg).size());
        strcpy(argv[iArg], qbalArgs.at(iArg).data());
    }

    if(pfOutput != nullptr)
        UTestFrameWorkBase::setTestOutputStream(pfOutput);

    int iActualExitCode = UTestFrameWorkBase::instance().exec(argc, &argv);

    UTestFrameWorkBase::deleteInstance();

    QVERIFY2(iExpectedExitCode == iActualExitCode,
             QByteArray("\n\tCommand line execution of \"" + qbalArgs.mid(1, -1).join(" ") + "\" has failed" +
                        "\n\tiActualExitCode\t" + QByteArray::number(iActualExitCode) +
                        "\n\tiExpectedExitCode\t" + QByteArray::number(iExpectedExitCode)));
}

void UT_UFrameWorkBase::tcCLInput()
{
    QFETCH(QList<QByteArrayList>, qlCommandLineArgument);
    QFETCH(int, iExpectedExitCode);
    QFETCH(QByteArray, qstrCommandLineOutputFile);
    QFETCH(QStringList, qstrlCommandLineOutput);
    QFETCH(URegExMap, qmFilter);

    FILE* pfOutput = fopen(qstrCommandLineOutputFile, "w");
    QVERIFY2(pfOutput != nullptr, QByteArray("\n\tCould not open file " + qstrCommandLineOutputFile));

    foreach(QByteArrayList qbalCommandLineArgument, qlCommandLineArgument)
        tcCLInput_execApplication(qbalCommandLineArgument, iExpectedExitCode, pfOutput);

    if(pfOutput)
        fclose(pfOutput);

    if(!qstrCommandLineOutputFile.isEmpty())
    {
        bool bNoError = true;
        QFile qFile(qstrCommandLineOutputFile);
        QVERIFY2((bNoError = qFile.open(QIODevice::ReadOnly)), QByteArray("\n\tCould not open file " + qstrCommandLineOutputFile));
        QStringList qstrlTestData;
        if(bNoError)
        {
            QTextStream qtsTestData(&qFile);
            qstrlTestData = qtsTestData.readAll().split('\n');
        }

        filterStringList(qstrlTestData, qmFilter);
        filterStringList(qstrlCommandLineOutput, qmFilter);

        QCOMPARE(qstrlTestData, qstrlCommandLineOutput);
    }
}

void UT_UFrameWorkBase::initTestCase()
{
    bool bNoError = true;
    QDir qDir;
    if(qDir.cd("log"))
    {
        QVERIFY2((bNoError = qDir.removeRecursively()), "Could not remove \"log\"-folder");
        if(bNoError)
            qDir.cdUp();
    }
    if(qDir.cd("test"))
    {
        QVERIFY2((bNoError = qDir.removeRecursively()), "Could not remove \"test\"-folder");
        if(bNoError)
            qDir.cdUp();
    }
    QVERIFY2(qDir.mkdir("test"), "Could not create \"./test/\"");
}

void UT_UFrameWorkBase::init()
{
}

void UT_UFrameWorkBase::cleanup()
{
    if(QString(QTest::currentTestFunction()).startsWith("tc_CLInput"))
    {
        if(!QTest::currentTestFailed())
        {
            QString qstrFile(QTest::currentDataTag());
            qstrFile.replace(QChar(' '), QChar('_'));
            qstrFile.replace(QChar('/'), QChar('_'));
            if(QTest::currentDataTag() == QString("config -etest/tc_config_-e.txt") ||
               QTest::currentDataTag() == QString("config --exporttest/tc_config_--export.txt"))
            {
                qstrFile = qstrFile.split("test").first();
            }
            qstrFile.prepend("test/tc_");
            qstrFile.append(".txt");
            if(!QFile::remove(qstrFile))
                qWarning() << QString("Could not delete file \"%1\"").arg(qstrFile).toUtf8();
        }
    }
    QSettings qSettings;
    qSettings.clear();
}

void UT_UFrameWorkBase::cleanupTestCase()
{
    //    QDir qDir;
    //    QVERIFY2(qDir.cd("test"), "Could not enter \"./test/\"");
    //    QVERIFY2(qDir.removeRecursively(), "Could not remove \"test\"-folder");
}

void UT_UFrameWorkBase::tc_CLInputImplShort_data()
{
    tcCLInput_dataSetColumn();

    QByteArray qbaPrefix, qbaArg, qbaSuffix;
    QByteArrayList qbalParameter;
    qbaPrefix = "";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << QStringList("")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"-\""
                    << ""
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "h";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "hh";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "hv";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << QString("UT-UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "h0";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"0\""
                    << ""
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "v";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UT-UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "vh";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << QString("UT-UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "-";
    qbaArg = "vv";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UT-UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }
}

void UT_UFrameWorkBase::tc_CLInputAutoGenShort_data()
{
    tc_CLInputImplShort_data();

    QByteArray qbaTestChar(tcCLInput_dataAutoChar());

    QStringList qstrlHelpOutput;

    //  "-h[0-9A-Za-z]"
    //  output content of "-h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help     Displays this help."
                        << "  -v, --version  Displays version information."
                        << "  --version-all  Displays versions of used libraries"
                        << ""
                        << "Arguments:"
                        << "  module         The options following the module will be applied to the"
                        << "                 module. List of modules: config"
                        << "";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, "-h");
    }

    //  "-[0-9A-Za-z]h"
    //  output content of "-h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help     Displays this help."
                        << "  -v, --version  Displays version information."
                        << "  --version-all  Displays versions of used libraries"
                        << ""
                        << "Arguments:"
                        << "  module         The options following the module will be applied to the"
                        << "                 module. List of modules: config"
                        << "";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, "-", "h");
    }

    //  "-v[0-9A-Za-z]"
    //  output content of "-h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help     Displays this help."
                        << "  -v, --version  Displays version information."
                        << "  --version-all  Displays versions of used libraries"
                        << ""
                        << "Arguments:"
                        << "  module         The options following the module will be applied to the"
                        << "                 module. List of modules: config"
                        << "";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, "-v");
    }

    //  "-[0-9A-Za-z]v"
    //  output content of "-h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help     Displays this help."
                        << "  -v, --version  Displays version information."
                        << "  --version-all  Displays versions of used libraries"
                        << ""
                        << "Arguments:"
                        << "  module         The options following the module will be applied to the"
                        << "                 module. List of modules: config"
                        << "";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, "-", "v");
    }

    //  "-[0-9A-Za-z]{1,2}"
    //  output content of "-h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help     Displays this help."
                        << "  -v, --version  Displays version information."
                        << "  --version-all  Displays versions of used libraries"
                        << ""
                        << "Arguments:"
                        << "  module         The options following the module will be applied to the"
                        << "                 module. List of modules: config"
                        << "";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 2);
    }
}

void UT_UFrameWorkBase::tc_CLInputImplLong_data()
{
    tcCLInput_dataSetColumn();

    QByteArray qbaPrefix, qbaArg, qbaSuffix;
    QByteArrayList qbalParameter;
    qbaPrefix = "--";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"--\""
                    << ""
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "--";
    qbaArg = "help";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "--";
    qbaArg = "version";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UT-UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "--";
    qbaArg = "version-all";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UT-UFrameWorkBase %1").arg(GIT_VERSION)
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")},
        {1, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }
}

void UT_UFrameWorkBase::tc_CLInputAutoGenLong_data()
{
    tc_CLInputImplLong_data();

    QByteArray qbaTestChar(tcCLInput_dataAutoChar());

    QStringList qstrlHelpOutput;
}

void UT_UFrameWorkBase::tc_CLInputImplShortM_config_data()
{
    tcCLInput_dataSetColumn();

    QByteArray qbaPrefix, qbaArg, qbaSuffix;
    QByteArrayList qbalParameter;
    qbaPrefix = "tst ";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"tst\""
                    << ""
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "tst -";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"tst\""
                    << ""
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"-\""
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "h";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "ha";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "h";
    qbaSuffix = "d";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "h";
    qbaSuffix = "e";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "h";
    qbaSuffix = "g";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "h";
    qbaSuffix = "i";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "hh";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "hr";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "h";
    qbaSuffix = "s";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "hv";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap{
        {16, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "v";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "va";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "v";
    qbaSuffix = "d";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "v";
    qbaSuffix = "e";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "v";
    qbaSuffix = "g";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "vh";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap{
        {16, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "v";
    qbaSuffix = "i";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "vr";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "v";
    qbaSuffix = "s";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "vv";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "a";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "ah";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "av";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "r";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "rh";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "rv";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "tst";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Parameter \"%1\" not found").arg(qbalParameter.join(" ").data())
                    << ""
                    << "All parameters and their values:"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "en-GB")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "268435455")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("%1/log").arg(QDir::currentPath()))
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "268435455")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "h";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter \"" + qbaSuffix + "\" not found"
                    << ""
                    << "All parameters and their values:"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "g";
    qbaSuffix = "v";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter \"" + qbaSuffix + "\" not found"
                    << ""
                    << "All parameters and their values:"
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << "logDir    :  /home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "e";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "e";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + ".txt").replace(' ', '_').replace("/", "_");
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + ".txt").replace(' ', '_').replace("/", "_")
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "s";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language=en-AT";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-AT"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "s";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail=65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=65535"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "s";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir=test";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/test"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "s";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel=65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=65535"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "d";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language" << "=" << "en-AT";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "d";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail" << "=" << "65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "d";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir" << "=" << "test";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "d";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel" << "=" << "65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "i";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Missing parameter for \"" + qbaPrefix + qbaArg + qbaSuffix + "\" option"
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "i";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "no.config";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config -";
    qbaArg = "i";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "exported.config";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << QByteArray("language=en-AT"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    )
                << 0
                << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_")
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }
}

void UT_UFrameWorkBase::tc_CLInputAutoGenShortM_config_data()
{
    tc_CLInputImplShortM_config_data();

    QByteArray qbaTestChar(tcCLInput_dataAutoChar());

    QStringList qstrlHelpOutput;
    QByteArray qbaPrefix;

    //  "config -h[0-9A-Za-z]"
    //  output content of "config -h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help                                 Displays this help."
                        << "  -v, --version                              show version of library"
                        << "                                             UFrameWorkBase"
                        << "  -a, --all, --show-all                      shows all values of configuration"
                        << "  -r, --recursive, --show-recursive          shows all values of configuration"
                        << "                                             recursivley"
                        << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                        << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                        << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                        << "                                             <NAME>"
                        << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                        << "  -i, --import <FILEPATH>                    import configuration from"
                        << "                                             <FILEPATH>"
                        << "";
        qbaPrefix = "config -";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, qbaPrefix + "h");
    }

    //  "config -[0-9A-Za-z]h"
    //  output content of "config -h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help                                 Displays this help."
                        << "  -v, --version                              show version of library"
                        << "                                             UFrameWorkBase"
                        << "  -a, --all, --show-all                      shows all values of configuration"
                        << "  -r, --recursive, --show-recursive          shows all values of configuration"
                        << "                                             recursivley"
                        << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                        << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                        << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                        << "                                             <NAME>"
                        << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                        << "  -i, --import <FILEPATH>                    import configuration from"
                        << "                                             <FILEPATH>"
                        << "";
        qbaPrefix = "config -";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, qbaPrefix, "h");
    }

    //  "config -v[0-9A-Za-z]"
    //  output content of "config -h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help                                 Displays this help."
                        << "  -v, --version                              show version of library"
                        << "                                             UFrameWorkBase"
                        << "  -a, --all, --show-all                      shows all values of configuration"
                        << "  -r, --recursive, --show-recursive          shows all values of configuration"
                        << "                                             recursivley"
                        << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                        << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                        << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                        << "                                             <NAME>"
                        << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                        << "  -i, --import <FILEPATH>                    import configuration from"
                        << "                                             <FILEPATH>"
                        << "";
        qbaPrefix = "config -";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, qbaPrefix + "v");
    }

    //  "config -[0-9A-Za-z]v"
    //  output content of "config -h"
    {
        qstrlHelpOutput.clear();
        qstrlHelpOutput << QString("Usage: %1 [options]").arg(QDir::currentPath())
                        << ""
                        << "Options:"
                        << "  -h, --help                                 Displays this help."
                        << "  -v, --version                              show version of library"
                        << "                                             UFrameWorkBase"
                        << "  -a, --all, --show-all                      shows all values of configuration"
                        << "  -r, --recursive, --show-recursive          shows all values of configuration"
                        << "                                             recursivley"
                        << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                        << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                        << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                        << "                                             <NAME>"
                        << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                        << "  -i, --import <FILEPATH>                    import configuration from"
                        << "                                             <FILEPATH>"
                        << "";
        qbaPrefix = "config -";
        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 1, qbaPrefix, "v");
    }

    //  "config -[0-9A-Za-z]{1,2}"
    //  output content of "config -h"
//    {
//        qstrlHelpOutput.clear();
//        qstrlHelpOutput << QString("Usage: %1 [options]").arg(QDir::currentPath())
//                        << ""
//                        << "Options:"
//                        << "  -h, --help                                 Displays this help."
//                        << "  -v, --version                              show version of library"
//                        << "                                             UFrameWorkBase"
//                        << "  -a, --all, --show-all                      shows all values of configuration"
//                        << "  -r, --recursive, --show-recursive          shows all values of configuration"
//                        << "                                             recursivley"
//                        << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
//                        << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
//                        << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
//                        << "                                             <NAME>"
//                        << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
//                        << "  -i, --import <FILEPATH>                    import configuration from"
//                        << "                                             <FILEPATH>"
//                        << "";
//        qbaPrefix = "config -";
//        tcCLInput_dataGenerator(qbaTestChar, m_qbalCheckedCLO, qstrlHelpOutput, 2, qbaPrefix);
//    }
}

void UT_UFrameWorkBase::tc_CLInputImplLongM_config_data()
{
    tcCLInput_dataSetColumn();

    QByteArray qbaPrefix, qbaArg, qbaSuffix;
    QByteArrayList qbalParameter;
    qbaPrefix = "tst --";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"tst\""
                    << ""
                    << QString("Usage: %1 [options] | module [module-options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help     Displays this help."
                    << "  -v, --version  Displays version information."
                    << "  --version-all  Displays versions of used libraries"
                    << ""
                    << "Arguments:"
                    << "  module         The options following the module will be applied to the"
                    << "                 module. List of modules: config"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 1
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Unknown option: \"--\""
                    << ""
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "help";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("Usage: %1 [options]").arg(QDir::currentPath())
                    << ""
                    << "Options:"
                    << "  -h, --help                                 Displays this help."
                    << "  -v, --version                              show version of library"
                    << "                                             UFrameWorkBase"
                    << "  -a, --all, --show-all                      shows all values of configuration"
                    << "  -r, --recursive, --show-recursive          shows all values of configuration"
                    << "                                             recursivley"
                    << "  -g, --get, --get-value <NAME>              get value of parameter <NAME>"
                    << "  -s, --set, --set-value <NAME>=<VALUE>      set <VALUE> of parameter <NAME>"
                    << "  -d, --default, --set-default-value <NAME>  set default value of parameter"
                    << "                                             <NAME>"
                    << "  -e, --export <FILEPATH>                    export configuration to <FILEPATH>"
                    << "  -i, --import <FILEPATH>                    import configuration from"
                    << "                                             <FILEPATH>"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "version";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << QString("UFrameWorkBase %1").arg(GIT_VERSION)
                    << "")
                << URegExMap{
        {0, QRegularExpression("^([!-/\\w]+\\s+\\d+\\.).*$")}};
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "all";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "show-all";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "recursive";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "show-recursive";
    qbaSuffix = "";
    qbalParameter = QByteArrayList();
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "Parameter :  Value"
                    << "language  :  en-GB"
                    << "logDetail :  268435455"
                    << QString("logDir    :  %1/log").arg(QDir::currentPath())
                    << "logLevel  :  268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "get";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "en-GB")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "get-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "en-GB")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "export";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + ".txt").replace(' ', '_').replace("/", "_");
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter))
                << 0
                << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + ".txt").replace(' ', '_').replace("/", "_")
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language=en-AT";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-AT"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail=65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=65535"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir=test";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/test"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel=65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=65535"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language=en-AT";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-AT"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail=65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=65535"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir=test";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/test"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel=65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter)
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=65535"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "default";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language" << "=" << "en-AT";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "default";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail" << "=" << "65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "default";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir" << "=" << "test";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "default";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel" << "=" << "65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-default-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "language" << "=" << "en-AT";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-default-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDetail" << "=" << "65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-default-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logDir" << "=" << "test";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "set-default-value";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "logLevel" << "=" << "65535";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << qbalParameter.join())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << qbalParameter.first())
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')))
                << 0
                << QByteArray("test/tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_')
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }

    qbaPrefix = "config --";
    qbaArg = "import";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "exported.config";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << QByteArray("language=en-AT"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    )
                << 0
                << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_")
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }
}

void UT_UFrameWorkBase::tc_CLInputAutoGenLongM_config_data()
{
    tc_CLInputImplLongM_config_data();

    QByteArray qbaTestChar(tcCLInput_dataAutoChar());

    QStringList qstrlHelpOutput;
}

void UT_UFrameWorkBase::tc_CLInputSingle_data()
{
    tcCLInput_dataSetColumn();

    QByteArray qbaPrefix, qbaArg, qbaSuffix;
    QByteArrayList qbalParameter;

    qbaPrefix = "config --";
    qbaArg = "import";
    qbaSuffix = "";
    qbalParameter = QByteArrayList() << "exported.config";
    {
        QTest::newRow(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "))
                << (QList<QByteArrayList>()
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "s"
                        << QByteArray("language=en-AT"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + qbaArg + qbaSuffix
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    << (QByteArrayList()
                        << qbaPrefix.split(' ').mid(0, qbaPrefix.split(' ').size() - 1)
                        << qbaPrefix.split(' ').last() + "e"
                        << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_"))
                    )
                << 0
                << QByteArray("test/") + QByteArray("tc_" + qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" ") + ".txt").replace(' ', '_').replace("/", "_")
                << (QStringList()
                    << "[General]"
                    << "language=en-GB"
                    << "logDetail=268435455"
                    << "logDir=/home/uam/Documents/charForgeRedesign/build-DevCharForge-Desktop_Qt_5_12_7_GCC_64bit-Debug/UT-UFrameWorkBase/log"
                    << "logLevel=268435455"
                    << "")
                << URegExMap();
        m_qbalCheckedCLO.append(qbaPrefix + qbaArg + qbaSuffix + qbalParameter.join(" "));
    }
}


QTEST_APPLESS_MAIN(UT_UFrameWorkBase)
