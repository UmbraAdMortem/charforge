﻿#ifndef ULOG_H
#define ULOG_H

//#include <utility/abstracts/UAbstractCommandLineParser.h>
//#include <utility/abstracts/uabstractsConfiguration.h>
#include <utility/exports/uBaseUtility.h>

#include <QDebug>
#include <QDir>

namespace base {

namespace internal {

class ULog : public QObject, public UBaseUtility
{
    Q_OBJECT

    Q_PROPERTY(QString logDir READ logDir WRITE setLogDir RESET resetLogDir NOTIFY logDirChanged USER true FINAL)
    Q_PROPERTY(ULogLevels logLevel READ logLevel WRITE setLogLevel RESET resetLogLevel NOTIFY logLevelChanged USER true FINAL)
    Q_PROPERTY(ULogDetails logDetail READ logDetail WRITE setLogDetail RESET resetLogDetail NOTIFY logDetailChanged USER true FINAL)

public:
    enum ULogLevel
    {
        ULogLNone               =   0x0000000,
        ULogLDebug              =   0x0000001,
        ULogLInfo               =   0x0000002,
        ULogLWarning            =   0x0000004,
        ULogLCritical           =   0x0000008,
        ULogLFatal              =   0x0000010,

        ULogLCategoryDefault    =   0x8000000,
        ULogLAll                =   0xFFFFFFF
    };
    Q_DECLARE_FLAGS(ULogLevels, ULogLevel)
    Q_FLAG(ULogLevels)
    enum ULogDetail
    {
        ULogDNone               =   0x0000000,
        ULogDCategory           =   0x0000001,
        ULogDFile               =   0x0000002,
        ULogDFunction           =   0x0000004,
        ULogDLine               =   0x0000008,

        ULogDNoCategory         =   0xFFFFFFE,
        ULogDAll                =   0xFFFFFFF
    };
    Q_DECLARE_FLAGS(ULogDetails, ULogDetail)
    Q_FLAG(ULogDetails)

    static ULog& instance();
    static void deleteInstance();
    ~ULog();

    ULog(const ULog&) = delete;
    ULog& operator=(const ULog) = delete;

    static void logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &qstrMsg);

    int activateLogging();
    void deactivateLogging();

    //  getter
    ULogLevels logLevel() const { return s_logLevel; }
    ULogDetails logDetail() const { return s_logDetail; }
    QString logFile() const { return s_qstrCurrentLogFile; }
    QString logDir() const { return s_qstrCurrentLogDir; }

private:
    ULog();

    QString logFileName();

    //  varialbes
    static ULog* m_pInstance;
    static QtMessageHandler s_qmhPreviousMsgHandler;
    static bool s_bLogging;
    static QFile* s_qfLogFile;
    static ULogLevels s_logLevel;
    static ULogDetails s_logDetail;
    static QString s_qstrCurrentLogFile;
    static QString s_qstrCurrentLogDir;

public slots:
    void setLogLevel(ULogLevels logLevel);
    void setLogDetail(ULogDetails logDetail);
    void setLogDir(QString qstrLogDir);

    void resetLogLevel() { setLogLevel(ULogLAll); }
    void resetLogDetail() { setLogDetail(ULogDAll); }
    void resetLogDir() { setLogDir(QDir::currentPath() + QDir::separator() + "log"); }

signals:
    void logDirChanged(QString qstrLogDir);
    void logLevelChanged(ULogLevels logLevel);
    void logDetailChanged(ULogDetails logDetail);
};

} // namespace internal

} // namespace base

#endif // ULOG_H
