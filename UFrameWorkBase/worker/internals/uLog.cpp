﻿#include "uLog.h"

#include <QDateTime>

#define ULOG_CONFIG_LENGTH 28
#define ULOG_CONFIG_BASE 2
#define ULOG_CONFIG_FILL QChar('0')

using namespace base::internal;

ULog* ULog::m_pInstance = nullptr;
QtMessageHandler ULog::s_qmhPreviousMsgHandler = nullptr;
bool ULog::s_bLogging = false;
QFile* ULog::s_qfLogFile = new QFile();
ULog::ULogLevels ULog::s_logLevel = ULogLAll;
ULog::ULogDetails ULog::s_logDetail = ULogDAll;
QString ULog::s_qstrCurrentLogFile = "";
QString ULog::s_qstrCurrentLogDir = "";

ULog& ULog::instance()
{
    if(m_pInstance == nullptr)
        m_pInstance = new ULog();
    return *m_pInstance;
}

void ULog::deleteInstance()
{
    if(m_pInstance)
    {
        delete m_pInstance;
        m_pInstance = nullptr;
    }
}

ULog::~ULog()
{
    deactivateLogging();
}

void ULog::logMsgHandler(QtMsgType type, const QMessageLogContext& context, const QString& qstrMsg)
{
    if(!s_bLogging)
        return (*s_qmhPreviousMsgHandler)(type, context, qstrMsg);
    if(((1 << static_cast<int>(type)) & s_logLevel) != (1 << static_cast<int>(type)))
        return;

    QTextStream qtsLog(s_qfLogFile);
    qtsLog << QDateTime::currentDateTime().toString(Qt::ISODateWithMs);
    switch (type)
    {
    case QtDebugMsg:    qtsLog << " DBG "; break;
    case QtInfoMsg:     qtsLog << " INF "; break;
    case QtWarningMsg:  qtsLog << " WRN "; break;
    case QtCriticalMsg: qtsLog << " CRT "; break;
    case QtFatalMsg:    qtsLog << " FTL "; break;
    default:
        qWarning() << "Unhandled QtMsgType "
                   << QDateTime::currentDateTime().toString(Qt::ISODateWithMs) << endl
                   << " TYPE:" << static_cast<int>(type) << endl
                   << " CATEGORY: " << context.category << endl
                   << " FILE: " << context.file << endl
                   << " FUNCTION: " << context.function << endl
                   << " LINE: " << context.line << endl
                   << " MSG: " << qstrMsg << endl;
        return (*s_qmhPreviousMsgHandler)(type, context, qstrMsg);
    }

    qtsLog << " MSG: " << qstrMsg;

    if(s_logDetail != ULogDNone)
    {
        if((s_logDetail & ULogDCategory) == ULogDCategory)
            qtsLog << " CATEGORY: " << context.category;
        if((s_logDetail & ULogDFile) == ULogDFile)
            qtsLog << " FILE: " << context.file;
        if((s_logDetail & ULogDFunction) == ULogDFunction)
            qtsLog << " FUNCTION: " << context.function;
        if((s_logDetail & ULogDLine) == ULogDLine)
            qtsLog << " LINE: " << QString("%1 ").arg(context.line, 10);
    }

    qtsLog << endl;
    qtsLog.flush();
}

int ULog::activateLogging()
{
    syncConfiguration(this);

    QDir qLogDir(s_qstrCurrentLogDir);
    if(!qLogDir.exists())
        if(!qLogDir.mkpath(qLogDir.absolutePath()))
            return -1;

    s_qfLogFile->setFileName(qLogDir.filePath(logFileName()));
    if(!s_qfLogFile->open(QIODevice::Append | QIODevice::Text))
        return -2;

    QtMessageHandler qmhTemp = qInstallMessageHandler(logMsgHandler);
    if(qmhTemp != logMsgHandler)
        s_qmhPreviousMsgHandler = qmhTemp;

    s_bLogging = true;

    QTextStream qtsLog(s_qfLogFile);
    qtsLog << endl;
    qtsLog.flush();

    qInfo() << QString("Logger activated, LogLevel: 0b%1 LogDetail 0b%2")
                .arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL)
                .arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    return 0;
}

void ULog::deactivateLogging()
{
    qInfo() << QString("Logger deactivated, LogLevel: 0b%1 LogDetail 0b%2")
                .arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL)
                .arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    s_bLogging = false;
    s_qfLogFile->close();
    qInstallMessageHandler(s_qmhPreviousMsgHandler);
    s_qmhPreviousMsgHandler = nullptr;
}

ULog::ULog()
{

}

QString ULog::logFileName()
{
    return QDateTime::currentDateTime().toString(Qt::ISODate).replace(":", "-") + ".log";
}


void ULog::setLogLevel(ULogLevels logLevel)
{
    QObject* pObject = this->sender();
    if(s_logLevel == logLevel && configObject()->contains("logLevel"))
        return;

    s_logLevel = logLevel;
    qInfo() << QString("Changing LogLevel to: 0b%1").arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    emit logLevelChanged(s_logLevel);
}

void ULog::setLogDetail(ULogDetails logDetail)
{
    QObject* pObject = this->sender();
    if(s_logDetail == logDetail && configObject()->contains("logDetail"))
        return;

    s_logDetail = logDetail;
    qInfo() << QString("Changing LogDetail to: 0b%1").arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    emit logDetailChanged(s_logDetail);
}

void ULog::setLogDir(QString qstrLogDir)
{
    QObject* pObject = this->sender();
    if(qstrLogDir == s_qstrCurrentLogDir && configObject()->contains("logDir"))
        return;

    QString qstrOldLogDir = s_qstrCurrentLogDir;
    QDir qLogDir(qstrLogDir);
    if(!s_qstrCurrentLogDir.isEmpty())
        qInfo() << QString("Changing LogDirectory to: %1").arg(qLogDir.absolutePath());
    s_qstrCurrentLogDir = qLogDir.absolutePath();
    emit logDirChanged(s_qstrCurrentLogDir);

    if(!qLogDir.exists())
        if(!qLogDir.mkpath(qLogDir.absolutePath()))
        {
            qWarning() << QString("Could not create LogDirectory: %1").arg(qLogDir.absolutePath());
            qWarning() << QString("LogDirectory unchanged: %1").arg(s_qfLogFile->fileName());
            return;
        }

    if(s_bLogging)
    {
        qInfo() << "Stop logging";
        s_bLogging = false;
        s_qfLogFile->close();
        if(s_qfLogFile->isOpen())
        {
            s_bLogging = true;
            qInfo() << "Continue logging";
            qWarning() << QString("Could not close: %1").arg(s_qfLogFile->fileName());
            qWarning() << QString("LogDirectory unchanged: %1").arg(s_qfLogFile->fileName());
            s_qstrCurrentLogDir = qstrOldLogDir;
            emit logDirChanged(s_qstrCurrentLogDir);
            return;
        }

        if(s_qfLogFile->copy(qLogDir.absolutePath() + QDir::separator() + s_qstrCurrentLogFile))
            if(s_qfLogFile->remove())
                s_qfLogFile->setFileName(s_qstrCurrentLogDir + QDir::separator() + s_qstrCurrentLogFile);

        if(!s_qfLogFile->open(QIODevice::Append | QIODevice::Text))
            return;

        s_bLogging = true;
        qInfo() << "Continue logging";
        qInfo() << QString("LogDirectory changed to: %1").arg(s_qfLogFile->fileName());
        qInfo() << QString("Logger activated, LogLevel: 0b%1 LogDetail 0b%2")
                    .arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL)
                    .arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    }
}
