﻿#ifndef UabstractsWORKER_H
#define UabstractsWORKER_H

#include <uBase_global.h>
#include <worker/exports/uInternalWorker.h>

namespace base {

class UFRAMEWORKBASE_EXPORT UBaseWorker : public internal::UAbstractConfiguration
{
public:
    UBaseWorker();
};

} // namespace base

#endif // UabstractsWORKER_H
