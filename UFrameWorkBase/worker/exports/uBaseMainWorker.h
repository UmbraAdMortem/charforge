﻿#ifndef UBASEMAINWORKER_H
#define UBASEMAINWORKER_H

#include <uBase_global.h>
#include <worker/exports/uInternalMainWorker.h>

namespace base {

class UFRAMEWORKBASE_EXPORT UBaseMainWorker : public internal::UAbstractMainWorker
{
protected:
    explicit UBaseMainWorker();

private:
    virtual void preExec();
    virtual void postExec();

    virtual inline const QString organisationName() = 0;
    virtual inline const QString applicationName() = 0;
    virtual inline const QString applicationVersion() = 0;
    virtual inline const QString applicationDescription() = 0;
};

} // namespace base

#endif // UBASEMAINWORKER_H
