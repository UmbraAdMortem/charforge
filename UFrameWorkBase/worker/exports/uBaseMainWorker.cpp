﻿#include "uBaseMainWorker.h"

using namespace base;

UBaseMainWorker::UBaseMainWorker()
{

}

void UBaseMainWorker::preExec()
{

}

void UBaseMainWorker::postExec()
{
    QMetaObject::invokeMethod(QCoreApplication::instance(), "quit", Qt::QueuedConnection);
}
