﻿#ifndef UABSTRACTMAINWORKER_H
#define UABSTRACTMAINWORKER_H

#include <QObject>
#include <utility/abstracts/uAbstractCommandLineParser.h>
#include <utility/abstracts/uAbstractConfiguration.h>

namespace base {

namespace internal {

/**
 * @brief The UAbstractMainWorker class
 * @note bug fixed: must be derived publicly from UAbstractCommandLineParser for cast at
 *                  UAbstractMainWorker::showVersion to work
 */
class UAbstractMainWorker : public QObject, public UAbstractCommandLineParser, public UAbstractConfiguration
{
    Q_OBJECT
    Q_PROPERTY(QString language READ language WRITE setLanguage RESET resetLanguage NOTIFY languageChanged USER true FINAL)

public:
    int exec(int& argc, char** argv[]);

    static void showVersion(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void showAllVersions(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);

    QString language() const { return m_qstrLanguage; }

    const QString versionString() override;

    static void setTestOutputStream(FILE* pfTestOutputStream) { s_pfTestOutputStream = pfTestOutputStream; }
    static FILE* testOutputStream() { return s_pfTestOutputStream; }

protected:
    explicit UAbstractMainWorker();
    ~UAbstractMainWorker();

private:
    void addMainCommandLineOptions();
    inline const QString objectName() final { return QObject::objectName(); }

    virtual void preExec() = 0;
    virtual void internalPostExec();
    virtual void postExec() = 0;

    virtual inline const QString organisationName() = 0;
    virtual inline const QString applicationName() = 0;
    virtual inline const QString applicationVersion() = 0;
    virtual inline const QString applicationDescription() = 0;

    QString versionStringAllLibs();

    QString m_qstrLanguage;

    static FILE* s_pfTestOutputStream;

public slots:
    void setLanguage(QString qstrLanguage);
    void resetLanguage() { setLanguage("en-GB"); }

signals:
    void callPostExec();
    void languageChanged(QString qstrLanguage);
};

} // namespace base

} // namespace internal

#endif // UABSTRACTMAINWORKER_H
