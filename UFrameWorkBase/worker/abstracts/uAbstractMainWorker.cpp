﻿#include "uAbstractMainWorker.h"

#include <QCoreApplication>
#include <QDebug>
#include <QMetaProperty>

#include <worker/internals/uLog.h>

using namespace base::internal;

//#define SETTINGS_PREFIX ""
//#define SETTINGS_DEFAULT QVariantMap qvmSettingsDefault{ \
//{"Locale", QVariant("en-GB")}, \
//{"LogPath", QVariant(ULogger::instance().logDir())}, \
//{"LogLevel", QVariant(ULogger::instance().logLevel())}, \
//{"LogDetail", QVariant(ULogger::instance().logDetail())} }
FILE* UAbstractMainWorker::s_pfTestOutputStream = nullptr;

int UAbstractMainWorker::exec(int& argc, char** argv[])
{
    //  initialize application parameter
    QCoreApplication* pApp = new QCoreApplication(argc, *argv);
    pApp->setOrganizationName(organisationName());
    pApp->setApplicationName(applicationName());
    pApp->setApplicationVersion(applicationVersion());

    addMainCommandLineOptions();

//    UAbstractSettings settings(this);
    syncConfiguration(this);
    addCommandLineParser(configObject());

    ULog::instance().activateLogging();
    addCommandLineParser(ULog::instance().configObject());

    preExec();

    emit callPostExec();

    return QCoreApplication::exec();
}

void UAbstractMainWorker::showVersion(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    Q_UNUSED(qstrlCommandLineOptionNames)

    UAbstractMainWorker* pWorker;
    if((pWorker = dynamic_cast<UAbstractMainWorker*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UAbstractMainWorker";
        QCoreApplication::exit(EXIT_FAILURE);
    }

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    qtsStdOut << pWorker->versionString();
    qtsStdOut.flush();
    QCoreApplication::exit(EXIT_SUCCESS);
}

void UAbstractMainWorker::showAllVersions(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    Q_UNUSED(qstrlCommandLineOptionNames)

    UAbstractMainWorker* pWorker;
    if((pWorker = dynamic_cast<UAbstractMainWorker*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UAbstractMainWorker";
        QCoreApplication::exit(EXIT_FAILURE);
    }

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    qtsStdOut << pWorker->versionStringAllLibs();
    qtsStdOut.flush();
    QCoreApplication::exit(EXIT_SUCCESS);
}

const QString UAbstractMainWorker::versionString()
{
    return QString(applicationName() + QLatin1Char(' ') +
                   applicationVersion() + QLatin1Char('\n'));
}

UAbstractMainWorker::UAbstractMainWorker()
{
    connect(this, &UAbstractMainWorker::callPostExec,
            this, &UAbstractMainWorker::internalPostExec,
            Qt::QueuedConnection);
}

UAbstractMainWorker::~UAbstractMainWorker()
{
    ULog::deleteInstance();
}

void UAbstractMainWorker::addMainCommandLineOptions()
{
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  addVersionOption(),
                  &showVersion));

    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "version-all",
                                     "Displays versions of used libraries"),
                  &showAllVersions));
}

void UAbstractMainWorker::internalPostExec()
{
    processCommandLineArguments();

    postExec();
}

QString UAbstractMainWorker::versionStringAllLibs()
{
    QString qstrVersionsString(versionString());
    for(QHash<QString, UAbstractCommandLineParser*>::ConstIterator qhiCommandLineParser = m_qhCommandLineParser.constBegin();
        qhiCommandLineParser != m_qhCommandLineParser.constEnd();
        qhiCommandLineParser++)
    {
        qstrVersionsString.append(qhiCommandLineParser.value()->versionString());
    }
    return qstrVersionsString;
}

void UAbstractMainWorker::setLanguage(QString qstrLanguage)
{
    QObject* pObject = this->sender();
    if (m_qstrLanguage == qstrLanguage && configObject()->contains("language"))
        return;

    //  NOTE: change language of application

    m_qstrLanguage = qstrLanguage;
    emit languageChanged(m_qstrLanguage);
}


