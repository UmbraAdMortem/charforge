﻿#ifndef UBASEMODULE
#define UBASEMODULE

#include <uBase_global.h>
#include <module/exports/uInternalModule.h>

namespace base {

class UFRAMEWORKBASE_EXPORT UBaseModule : public internal::UAbstractCommandLineParser
{
public:
    UBaseModule();
};

} // namespace base

#endif // UBASEMODULE
