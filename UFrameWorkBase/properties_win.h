﻿#ifndef __PROPERTIES_WIN_H__
#define __PROPERTIES_WIN_H__

#include "gitInfo.h"

#define PROP_OS               VOS__WINDOWS32
#define PROP_FILETYPE         VFT_APP

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

//  Define output vars
#ifndef VER_COMMIT
  #define VERSION	                STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_PATCH) ".0"
  #define VER_FILEVERSION         VER_MAJOR, VER_MINOR, VER_PATCH, 0
  #define VER_FILEVERSION_STR     STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_PATCH) ".0"
#else
  #define VERSION	                STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_PATCH) "." STR(VER_COMMIT)
  #define VER_FILEVERSION         VER_MAJOR, VER_MINOR, VER_PATCH, VER_COMMIT
  #define VER_FILEVERSION_STR     STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_PATCH) "." STR(VER_COMMIT)
#endif
#define VER_PRODUCTVERSION        VER_MAJOR, VER_MINOR, VER_PATCH
#define VER_PRODUCTVERSION_STR    STR(VER_MAJOR) "." STR(VER_MINOR) "." STR(VER_PATCH)

#ifndef DEBUG
  #define FLAG_DEBUG                   0
#else
  #define FLAG_DEBUG                   VS_FF_DEBUG
#endif

#ifndef VER_PATCHED
  #define FLAG_PATCHED                 0
#else
  #define FLAG_PATCHED                 VS_FF_PATCHED
#endif

#ifndef VER_COMMIT
  #define FLAG_PRERELEASE              0
#else
  #define FLAG_PRERELEASE              VS_FF_PRERELEASE
#endif

// #ifndef VER_PRIVATEBUILD_STR
//   #define FLAG_PRIVATEBUILD            0
// #else
  #define FLAG_PRIVATEBUILD            VS_FF_PRIVATEBUILD
// #endif

#ifndef VER_SPEZIALBUILD_STR
  #define FLAG_SPEZIALBUILD            0
#else
  #define FLAG_SPEZIALBUILD            VS_FF_SPECIALBUILD
#endif

#endif  //__PROPERTIES_WIN_H__
