#!"C:/Program\ Files/Git/usr/bin/perl"; usr/bin/perl
use warnings;
use strict;

my $debug = 0;
my $test  = 0;
my $quiet = 0;
my $outputFile = '';
my $help = <<'EOF';
usage: [--help] | [--quiet] [OUT FILE]

When called without arguments version information writes to console.

--help      - displays this output.

--quiet     - Suppress console output.

--test      - Takes "10.5.1-comment-28-g991fe07" as test output of git describe

OUT FILE    - Path to writable file that is included in the project's rc file.

Version information is expected to be in the format: Major.Minor.Fix[-Comment]
eg.: 10.5.1-comment

Example pre-build event:
MSVS:
"C:/Program Files/Git/usr/bin/perl.exe" "$(ProjectDir)GIT-VS-VERSION-GEN.pl" --quiet "$(ProjectDir)gitVersion.h"
QT Creator:
Command:            perl
Arguments:          gitInfo.pl --quiet gitInfo.h
Working directory:  %{sourceDir}
EOF

# Check ARGS
# print '@ARGV:', "\n@ARGV\n", '@ARGV END', "\n";
for(@ARGV)
{
  m/\Q--debug/i and $debug = 1;
  m/\Q--quiet/i and $quiet = 1;
  m/\Q--test/i and $test = 1;
  if(m/\Q--help/i)
  {
    print $help;
    exit 0;
  }
}
if(scalar @ARGV > 0 and $ARGV[-1] !~ m/\Q--quiet/i and $ARGV[-1] !~ m/\Q--help/i and $ARGV[-1] !~ m/\Q--debug/i)
{
  $outputFile = $ARGV[-1];
  chomp $outputFile;
  !$debug or print '$outputFile:', "$outputFile:\n";
}

# Initialize Variables
my $verMajor = 0;
my $verMinor = 0;
my $verPatch = 0;
my $verCommit = 0;
my $verComment = 'noVersion';
my $verHash = '';

# VS_FF_PATCHED, dirty branch
my $verPatched = "";
# VS_FF_PRIVATEBUILD, not build using a CI pipeline
my $verPrivateBuild = "";
# VS_FF_SPEZIALBUILD, branch name if not "master"
my $verSpecialBuild = "master";


# Git core command
my $gitBaseCommand = "";
!$debug or print '$OSNAME:', "$^O:\n";
if(($^O cmp 'linux') == 0)
{
  !$debug or print "is Linux\n";
  $gitBaseCommand = "git";
} elsif(($^O cmp 'darwin') == 0)
{
  !$debug or die "is Mac OS X\n";
} elsif(($^O cmp 'MSWin32') == 0)
{
  !$debug or print "is Windows 32 bit\n";
  $gitBaseCommand = "\"C:\\Program Files\\Git\\bin\\git\"";
} else
{
  !$debug or die "is something else: $^O\n";
}

# Get version string
my $gitVersion = `$gitBaseCommand describe --long --always --tags` or die $!;
chomp $gitVersion;
!$debug or print '$gitVersion:', "$gitVersion:\n";

# Parse $gitVersion
!$test or $gitVersion = '10.5.1-comment-28-g991fe07';
$gitVersion =~ m#^(?<major>\d+)[_\.](?<minor>\d+)[_\.]?(?<patch>\d+)?-?(?<comment>[A-Za-z][\w\s]*)?-(?<commit>\d+)?-?(?<hash>.+)$#;

!$debug or print '$+{major}:', "$+{major}:\n";
!$debug or print '$+{minor}:', "$+{minor}:\n";
!$debug or print '$+{patch}:', "$+{patch}:\n";
!$debug or print '$+{comment}:', "$+{comment}:\n";
!$debug or print '$+{commit}:', "$+{commit}:\n";
!$debug or print '$+{hash}:', "$+{hash}:\n\n";

$+{major} and $verMajor = $+{major};
$+{minor} and $verMinor = $+{minor};
$+{patch} and $verPatch = $+{patch};
($+{comment} and $verComment = $+{comment}) or $verComment = '';
$+{commit} and $verCommit = $+{commit};
$+{hash} and $verHash = $+{hash};

# Check if repository is dirty
$verPatched = `$gitBaseCommand diff-index --name-only HEAD`;
chomp $verPatched;
$verPatched and $verPatched = "-dirty";

$verPrivateBuild = `$gitBaseCommand config --global user.name`;
chomp $verPrivateBuild;

$verSpecialBuild = `$gitBaseCommand branch --show-current`;
chomp $verSpecialBuild;

!$debug or print '$gitVersion:', "$gitVersion:\n";
!$debug or print '$verMajor:', "$verMajor:\n";
!$debug or print '$verMinor:', "$verMinor:\n";
!$debug or print '$verPatch:', "$verPatch:\n";
!$debug or print '$verComment:', "$verComment:\n";
!$debug or print '$verCommit:', "$verCommit:\n";
!$debug or print '$verHash:', "$verHash:\n";
!$debug or print '$verPatched:', "$verPatched:\n";
!$debug or print '$verPrivateBuild:', "$verPrivateBuild:\n";
!$debug or print '$verSpecialBuild:', "$verSpecialBuild:\n\n";

my $includeGuard = "__GITINFO_H__";
if($outputFile)
{
  $includeGuard = uc "__$outputFile\__";
  $includeGuard =~ s#\.#_#g;
}

# Create output
my $outPut = <<EOF;
\/*************************************************************************
 * Autocreated header file by gitInfo.pl
*************************************************************************\/
#ifndef $includeGuard
#define $includeGuard

#define VER_MAJOR             $verMajor
#define VER_MINOR             $verMinor
#define VER_PATCH             $verPatch
#define VER_HASH              "$verHash\\0"
#define VER_PRIVATEBUILD_STR  "$verPrivateBuild\\0"
EOF

($verSpecialBuild cmp 'master') != 0 and  $outPut = qq{$outPut#define VER_SPEZIALBUILD_STR  "$verSpecialBuild\\0"\n};
$outPut = qq{$outPut#define VER_COMMENT_COMB      "$verHash$verPatched $verComment, built by $verPrivateBuild};
if(($verSpecialBuild cmp 'master') != 0)
{
  $outPut = qq{$outPut on $verSpecialBuild\\0"\n};
}
else
{
  $outPut = qq{$outPut\\0"\n};
}
$verCommit and  $outPut = qq{$outPut#define VER_COMMIT            $verCommit\n};
$verPatched and $outPut = qq{$outPut#define VER_PATCHED           "$verPatched\\0"\n};
$verComment and $outPut = qq{$outPut#define VER_COMMENT           "$verComment\\0"\n};
$outPut = "$outPut\n#endif  //$includeGuard\n";

# Write to file
if($outputFile)
{
  open FH, "> $outputFile" or die $!;
  print FH $outPut;
  close FH or die $!;
}

# Write to console
$quiet or print $outPut;
