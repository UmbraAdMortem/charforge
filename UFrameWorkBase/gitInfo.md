# Git - c++ - VERSIONINFO Generator

## Overview
This project uses an perl script to parse the output of git-describe and
generate a header to be included in a project's file version resource.

## Usage
This can be run from the command line.  It uses the following arguments:

usage: [--help] | [--quiet] [OUT FILE]

When called without arguments version information writes to console.

--help      - displays this output.

--quiet     - Suppress console output.

--test      - Takes "10.5.1-comment-28-g991fe07" as test output of git describe

OUT FILE    - Path to writable file that is included in the project's rc file.

Version information is expected to be in the format: Major.Minor.Fix[-Comment]
eg.: 10.5.1-comment

Example pre-build event:
MSVS:
"C:/Program Files/Git/usr/bin/perl.exe" "$(ProjectDir)GIT-VS-VERSION-GEN.pl" --quiet "$(ProjectDir)gitVersion.h"
QT Creator:
Command:            perl
Arguments:          gitInfo.pl --quiet gitInfo.h
Working directory:  %{sourceDir}
