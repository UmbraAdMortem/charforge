# Check if the config file exists
! include( ../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

QT -= gui

TEMPLATE = lib
DEFINES += UFRAMEWORKBASE_LIBRARY

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Generate gitInfo.h (versioning)
gitInfoTarget.target = gitInfo
gitInfoTarget.commands = echo "gitInfo.pl"; \
                         perl $$PWD/gitInfo.pl gitInfo.h
gitInfoTarget.depends = FORCE
QMAKE_EXTRA_TARGETS += gitInfoTarget
PRE_TARGETDEPS += gitInfo

HEADERS += \
    gitInfo.h \
    module/exports/uBaseModule.h \
    module/exports/uInternalModule.h \
    properties.h \
    uBase_global.h \
    utility/abstracts/uAbstractCommandLineParser.h \
    utility/abstracts/uAbstractConfiguration.h \
    utility/exports/uBaseUtility.h \
    utility/exports/uInternalUility.h \
    utility/internals/uConfiguration.h \
    worker/abstracts/uAbstractMainWorker.h \
    worker/exports/uBaseMainWorker.h \
    worker/exports/uBaseWorker.h \
    worker/exports/uInternalMainWorker.h \
    worker/exports/uInternalWorker.h \
    worker/internals/uLog.h

SOURCES += \
    module/exports/uBaseModule.cpp \
    utility/abstracts/uAbstractCommandLineParser.cpp \
    utility/abstracts/uAbstractConfiguration.cpp \
    utility/exports/uBaseUtility.cpp \
    utility/internals/uConfiguration.cpp \
    worker/abstracts/uAbstractMainWorker.cpp \
    worker/exports/uBaseMainWorker.cpp \
    worker/exports/uBaseWorker.cpp \
    worker/internals/uLog.cpp

TRANSLATIONS += \
    languages/UFrameWorkBase_en_AT.ts

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ../CHANGELOG.md \
    ../README.md
