﻿#ifndef UabstractsUTILITY_H
#define UabstractsUTILITY_H

#include <uBase_global.h>
#include <utility/exports/uInternalUility.h>

namespace base {

class UFRAMEWORKBASE_EXPORT UBaseUtility : public internal::UAbstractConfiguration
{
public:
    UBaseUtility();
};

} // namespace base

#endif // UabstractsUTILITY_H
