﻿#ifndef UABSTRACTCONFIGURATION_H
#define UABSTRACTCONFIGURATION_H

#include <utility/abstracts/uAbstractCommandLineParser.h>

#include <utility/internals/uConfiguration.h>

namespace base {

namespace internal {

/**
 * @brief The UAbstractSettings class
 * @note Uses Q_PROPERTY which must have
 *       Q_PROPERTY(type name
 *      (READ getFunction [WRITE setFunction] |
 *       MEMBER memberName [(READ getFunction | WRITE setFunction)])
 *       RESET resetFunction
 *       NOTIFY notifySignal
 *       USER bool)
 */
class UAbstractConfiguration
{
public:
    void syncConfiguration(QObject* pThis);

    bool isSyncActive() const { return (m_pConfiguration != nullptr); }

    UConfiguration* configObject() { return m_pConfiguration; }

protected:
    explicit UAbstractConfiguration();

private:
    UConfiguration* m_pConfiguration;
    bool m_bAlreadyConnected;
};

} // namespace internal

} // namespace base

#endif // UABSTRACTCONFIGURATION_H
