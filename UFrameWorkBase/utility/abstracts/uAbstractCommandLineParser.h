﻿#ifndef UABSTRACTCOMMANDLINEPARSER_H
#define UABSTRACTCOMMANDLINEPARSER_H

#include <QCommandLineParser>

#include <QFile>

namespace base {

namespace internal {

class UAbstractCommandLineParser: public QCommandLineParser
{
public:
    explicit UAbstractCommandLineParser();

    FILE* outputStream(FILE* pfOutputStream);

    void addPositionalArgument(const QString& qstrName, const QString& qstrDescription, const QString& qstrSyntax = QString());
    bool isPositionalArgumentDefined(const QString& qstrName) { return m_qstrlDefinedPositionalArguments.contains(qstrName); }
    bool process(const QStringList& qstrlArguments, bool bMain = true);

    static void showHelp(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void showHelpOnError(QStringList qstrlUnknownCLOptions, UAbstractCommandLineParser* pThis);

    virtual inline const QString versionString() { return QString(); }

protected:
    void addOption(QPair<QCommandLineOption, void(*)(QStringList, UAbstractCommandLineParser*)> qpCommandLineOption);
    void addOptions(QList<QPair<QCommandLineOption, void(*)(QStringList, UAbstractCommandLineParser*)>> qlCommandLineOptions);
    void addCommandLineParser(UAbstractCommandLineParser* pObject);
    void processCommandLineArguments();

    QCommandLineOption addVersionOption() { return QCommandLineParser::addVersionOption(); }

    QHash<QString, UAbstractCommandLineParser*> m_qhCommandLineParser;
    FILE* m_pfOutputStream = nullptr;

private:
    static void showHelpText(QStringList qstrlUnknownCLOptions, UAbstractCommandLineParser* pThis, bool bError);

    virtual inline const QString objectName() = 0;

    QList<QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>> m_qlCommandLineOptions;
    QStringList m_qstrlDefinedPositionalArguments;
};

} // namespace internal

} // namespace base

#endif // UABSTRACTCOMMANDLINEPARSER_H
