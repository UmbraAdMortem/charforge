﻿#include "uAbstractConfiguration.h"

#include <QDebug>
#include <QMetaProperty>
#include <QSignalMapper>

using namespace base::internal;

void UAbstractConfiguration::syncConfiguration(QObject* pThis)
{
    if(pThis == nullptr)
        return;

    if(m_pConfiguration == nullptr)
        m_pConfiguration = new UConfiguration(pThis);

    const QMetaObject* pMetaObjectOfParent = pThis->metaObject();

    for(int i = 1;
        i < pMetaObjectOfParent->propertyCount();
        ++i)
    {
        const QMetaProperty metaPropertyOfParent(pMetaObjectOfParent->property(i));
        if(metaPropertyOfParent.isReadable() &&
           metaPropertyOfParent.isWritable() &&
           metaPropertyOfParent.hasNotifySignal() &&
           metaPropertyOfParent.isResettable())
        {
            //  connect Q_PROPERTY with QSettings
            QString qstrPropertyName = metaPropertyOfParent.name();

            if(!m_bAlreadyConnected)
            {
                QByteArray qbaSignal("2");
                qbaSignal.append(metaPropertyOfParent.notifySignal().name());
                qbaSignal.append("(");
                qbaSignal.append(metaPropertyOfParent.notifySignal().parameterTypes().join(", "));
                qbaSignal.append(")");

                QSignalMapper* pMapper = new QSignalMapper(pThis);

                if(!pMapper->connect(pThis, qbaSignal.data(), pMapper, SLOT(map())))
                {
                    qWarning() << "Could not connect " << qbaSignal << " to UConfiguration";
                    continue;
                }
                pMapper->setMapping(pThis, qstrPropertyName);
                if(!pMapper->connect(pMapper, SIGNAL(mapped(QString)), m_pConfiguration, SLOT(valueChanged(QString))))
                {
                    qWarning() << "Could not connect " << qbaSignal << " to UConfiguration";
                    continue;
                }
                m_pConfiguration->registerPropertyObject(qstrPropertyName, pThis);
            }

            //  get settings
            QVariant qvValue = m_pConfiguration->QSettings::value(qstrPropertyName);
            if(qvValue.isNull())            //  if settings not available, use default(reset)
                metaPropertyOfParent.reset(pThis);
            else                            //  else set property
                pThis->setProperty(qstrPropertyName.toUtf8(), qvValue);
        }
    }
    m_bAlreadyConnected = true;
}

UAbstractConfiguration::UAbstractConfiguration()
{
    m_pConfiguration = nullptr;
    m_bAlreadyConnected = false;
}
