﻿#include "uAbstractCommandLineParser.h"

#include <QDebug>
#include <worker/abstracts/uAbstractMainWorker.h>

using namespace base::internal;

UAbstractCommandLineParser::UAbstractCommandLineParser()
{
    setOptionsAfterPositionalArgumentsMode(QCommandLineParser::ParseAsPositionalArguments);

    QCommandLineOption cloHelp(addHelpOption());
    m_qlCommandLineOptions.append(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(cloHelp, &showHelp));
}

FILE* UAbstractCommandLineParser::outputStream(FILE* pfOutputStream)
{
    FILE* pfTemp = UAbstractMainWorker::testOutputStream();
    if(QCoreApplication::applicationName().startsWith("UT-") && pfTemp != nullptr)
        return pfTemp;
    return pfOutputStream;
}

void UAbstractCommandLineParser::addPositionalArgument(const QString& qstrName, const QString& qstrDescription, const QString& qstrSyntax)
{
    QCommandLineParser::addPositionalArgument(qstrName, qstrDescription, qstrSyntax);
    m_qstrlDefinedPositionalArguments.append(qstrName);
}

/**
 * @brief UAbstractCommandLineParser::process
 * @param qstrlArguments
 * @param bMain
 * @return Returns false if encounters an error, otherwise true
 */
bool UAbstractCommandLineParser::process(const QStringList& qstrlArguments, bool bMain)
{
    if(!m_qhCommandLineParser.isEmpty() && !this->isPositionalArgumentDefined(QString("module")))
        addPositionalArgument("module",
                              QString("The options following the module will be applied to the module. List of modules: %1").arg(m_qhCommandLineParser.keys().join(", ")),
                              "| module [module-options]");
    if(!parse(qstrlArguments))
    {
        qCritical() << QString(errorText() + QLatin1Char('\n'));
        QCoreApplication::exit(EXIT_FAILURE);
    }

    //  Check CommandLineModules
    QStringList qstrlPositionalArguments = positionalArguments();
    if(!qstrlPositionalArguments.isEmpty())
    {
        if(m_qhCommandLineParser.contains(qstrlPositionalArguments.first()))
        {
            if(m_qhCommandLineParser.value(qstrlPositionalArguments.first()) != nullptr)
            {
                if(!m_qhCommandLineParser.value(qstrlPositionalArguments.first())->process(qstrlPositionalArguments, false))
                    return false;
            }
            else
            {
                qCritical() << QString("No command line parser for %1 available").arg(qstrlPositionalArguments.first());
                showHelpOnError(QStringList(), this);
            }
        }
        else
        {
            qCritical() << QString("Command line argument %1 not recognised").arg(qstrlPositionalArguments.first());
            showHelpOnError(qstrlPositionalArguments.mid(0, 1), this);
            return false;
        }
    }

    if(qstrlArguments.contains("--"))
    {
        qCritical() << QString("Command line argument \"--\" is no option");
        showHelpOnError(qstrlArguments.mid(1, 1), this);
        return false;
    }

    //  check if there are unknown options
    if(!this->unknownOptionNames().isEmpty())
    {
        qCritical() << QString("Command line argument %1 not recognised").arg(this->unknownOptionNames().join(", "));
        showHelpOnError(QStringList(), this);
        return false;
    }

    //  check CommandLineOptions
    bool bCommandLineOptionFound = false;
    for(int iCommandLineOption = 0; iCommandLineOption < m_qlCommandLineOptions.size(); iCommandLineOption++)
    {
        if(isSet(m_qlCommandLineOptions.at(iCommandLineOption).first))
        {
            m_qlCommandLineOptions.at(iCommandLineOption).second(m_qlCommandLineOptions.at(iCommandLineOption).first.names(), this);
            bCommandLineOptionFound = true;
        }
    }

    if(!bCommandLineOptionFound && !bMain)
    {
        showHelpOnError(QStringList(), this);
        return false;
    }

    return true;
}

void UAbstractCommandLineParser::showHelp(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    showHelpText(qstrlCommandLineOptionNames, pThis, false);
}

void UAbstractCommandLineParser::showHelpOnError(QStringList qstrlUnknownCLOptions, UAbstractCommandLineParser* pThis)
{
    showHelpText(qstrlUnknownCLOptions, pThis, true);
}

void UAbstractCommandLineParser::addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)> qpCommandLineOption)
{
    m_qlCommandLineOptions.append(qpCommandLineOption);
    QCommandLineParser::addOption(qpCommandLineOption.first);
}

void UAbstractCommandLineParser::addOptions(QList<QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)> > qlCommandLineOptions)
{
    m_qlCommandLineOptions.append(qlCommandLineOptions);
    for(int iOption = 0; iOption < qlCommandLineOptions.size(); iOption ++)
        QCommandLineParser::addOption(qlCommandLineOptions.at(iOption).first);
}

void UAbstractCommandLineParser::addCommandLineParser(UAbstractCommandLineParser* pObject)
{
    m_qhCommandLineParser.insert(pObject->objectName(), pObject);
}

void UAbstractCommandLineParser::processCommandLineArguments()
{
    process(QCoreApplication::arguments());
}

void UAbstractCommandLineParser::showHelpText(QStringList qstrlUnknownCLOptions, UAbstractCommandLineParser* pThis, bool bError)
{
    Q_UNUSED(qstrlUnknownCLOptions);

    QTextStream qtsCLOutput(pThis->outputStream(bError ? stderr : stdout));
    QString qstrHelpText = pThis->helpText();
    if(bError)
    {
        if(!qstrlUnknownCLOptions.isEmpty())
            qstrHelpText.prepend("Unknown option: \"" + qstrlUnknownCLOptions.join(" ") + "\"\n\n");
        else if(!pThis->unknownOptionNames().isEmpty())
            qstrHelpText.prepend("Unknown option: \"" + pThis->unknownOptionNames().join("") + "\"\n\n");
        else
            qstrHelpText.prepend("Missing parameter for \"" + QCoreApplication::arguments().mid(1, -1).join(" ") + "\" option\n\n");
    }

    qtsCLOutput << qstrHelpText;
    qtsCLOutput.flush();

    QCoreApplication::exit(bError ? EXIT_FAILURE : EXIT_SUCCESS);
}

