﻿#include "uConfiguration.h"

#include <QDebug>
#include <QMetaProperty>
#include <QFile>
#include <properties.h>
#include <QPointer>

using namespace base::internal;

QMap<QString, QPointer<QObject>> UConfiguration::s_qmPropertyObjects;

UConfiguration::UConfiguration(QObject* parent) : QSettings(parent)
{
    setObjectName("config");

    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "v" << "version",
                                     QString("show version of library ") + PROP_PRODUCTNAME),
                  &showVersion));

    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "a" << "all" << "show-all",
                                     "shows all values of configuration"),
                  &showAllValues));
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "r" << "recursive" << "show-recursive",
                                     "shows all values of configuration recursivley"),
                  &showAllValuesRecursive));
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "g" << "get" << "get-value",
                                     "get value of parameter "
                                     "<NAME>",
                                     "NAME"),
                  &getValue));
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "s" << "set" << "set-value",
                                     "set <VALUE> of parameter "
                                     "<NAME>",
                                     "NAME>=<VALUE"),
                  &setValue));
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "d" << "default" << "set-default-value",
                                     "set default value of parameter "
                                     "<NAME>",
                                     "NAME"),
                  &setDefaultValue));
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "e" << "export",
                                     "export configuration to "
                                     "<FILEPATH>",
                                     "FILEPATH"),
                  &exportConfig));
    addOption(QPair<QCommandLineOption, void (*)(QStringList, UAbstractCommandLineParser*)>(
                  QCommandLineOption(QStringList() << "i" << "import",
                                     "import configuration from "
                                     "<FILEPATH>",
                                     "FILEPATH"),
                  &importConfig));
}

void UConfiguration::registerPropertyObject(const QString& qstrProperty, QPointer<QObject> qpPropertyObject)
{
    s_qmPropertyObjects.insert(qstrProperty, qpPropertyObject);
}

void UConfiguration::showVersion(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    Q_UNUSED(qstrlCommandLineOptionNames)
    Q_UNUSED(pThis)

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    qtsStdOut << pThis->versionString();
    qtsStdOut.flush();
    QCoreApplication::exit(EXIT_SUCCESS);
}

void UConfiguration::showAllValues(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    showAllValues(qstrlCommandLineOptionNames, pThis, false);
}

void UConfiguration::showAllValuesRecursive(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    showAllValues(qstrlCommandLineOptionNames, pThis, true);
}

void UConfiguration::getValue(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    QString qstrParameter = pThis->value(qstrlCommandLineOptionNames.first());
    if(qstrParameter.isEmpty())
        return showHelpOnError(QStringList(), pThis);

    UConfiguration* pConfig;
    if((pConfig = dynamic_cast<UConfiguration*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UConfiguration";
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    QString qstrValue = pConfig->QSettings::value(qstrParameter).toString();
    if(qstrValue.isEmpty())
    {
        qCritical() << QString("Parameter \"%1\" not found").arg(qstrParameter);
        qtsStdOut << QString("Parameter \"%1\" not found\n\nAll parameters and their values:\n").arg(qstrParameter);
        qtsStdOut.flush();
        showAllValues(QStringList(), pThis);
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    qtsStdOut << qstrValue;
    qtsStdOut.flush();
    QCoreApplication::exit(EXIT_SUCCESS);
}

void UConfiguration::setValue(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    QString qstrParameter = pThis->value(qstrlCommandLineOptionNames.first());
    if(qstrParameter.isEmpty())
        return showHelpOnError(QStringList(), pThis);

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    QString qstrValue = qstrParameter.section("=", 1, -1);
    if(qstrValue.isEmpty())
    {
        qCritical() << QString("No value entered for &1").arg(qstrParameter);
        qtsStdOut << QString("No value entered for &1").arg(qstrParameter);
        qtsStdOut.flush();
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }
    qstrParameter = qstrParameter.section("=", 0, 0);
    if(qstrParameter.isEmpty())
    {
        qCritical() << "No parameter entered";
        qtsStdOut << "No parameter entered\n\n";
        qtsStdOut.flush();
        showAllValues(QStringList(), pThis);
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    UConfiguration* pConfig;
    if((pConfig = dynamic_cast<UConfiguration*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UConfiguration";
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }
    if(!pConfig->contains(qstrParameter))
    {
        qCritical() << QString("Parameter %1 not found").arg(qstrParameter);
        qtsStdOut << QString("Parameter %1 not found").arg(qstrParameter);
        qtsStdOut.flush();
        showAllValues(QStringList(), pThis);
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    QVariant qvValue(qstrValue);
    if(!qvValue.convert(pConfig->QSettings::value(qstrParameter).type()))
    {
        qCritical() << QString("Could not set \"%1\" to \"%2\"").arg(qstrParameter).arg(qvValue.toString());
        qtsStdOut << QString("Could not set \"%1\" to \"%2\"").arg(qstrParameter).arg(qvValue.toString());
        qtsStdOut.flush();
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    if(!s_qmPropertyObjects.value(qstrParameter).isNull())
        s_qmPropertyObjects.value(qstrParameter)->setProperty(qstrParameter.toUtf8().data(), qvValue);
    else
        QSettings().setValue(qstrParameter, qvValue);
//    pConfig->parent()->setProperty(qstrParameter.toUtf8().data(), qvValue);
}

void UConfiguration::setDefaultValue(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    QString qstrParameter = pThis->value(qstrlCommandLineOptionNames.first());
    if(qstrParameter.isEmpty())
        return showHelpOnError(QStringList(), pThis);

    UConfiguration* pConfig;
    if((pConfig = dynamic_cast<UConfiguration*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UConfiguration";
        QCoreApplication::exit(EXIT_FAILURE);
    }

    if(!s_qmPropertyObjects.value(qstrParameter).isNull())
    {
        QPointer<QObject> pQObject = s_qmPropertyObjects.value(qstrParameter);
        const QMetaObject* pMetaObject = s_qmPropertyObjects.value(qstrParameter)->metaObject();
        pMetaObject->property(pMetaObject->indexOfProperty(qstrParameter.toUtf8())).reset(s_qmPropertyObjects.value(qstrParameter));
    }
    else
        QSettings().remove(qstrParameter);

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    qtsStdOut << pConfig->parent()->property(qstrParameter.toUtf8()).toString();
    qtsStdOut.flush();
    QCoreApplication::exit(EXIT_SUCCESS);
}

void UConfiguration::exportConfig(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    QString qstrParameter = pThis->value(qstrlCommandLineOptionNames.first());
    if(qstrParameter.isEmpty())
        return showHelpOnError(QStringList(), pThis);

    UConfiguration* pConfig;
    if((pConfig = dynamic_cast<UConfiguration*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UConfiguration";
        QCoreApplication::exit(EXIT_FAILURE);
        return;
    }

    //  TODO:   Use correct prefix for configuration
    QString qstrPrefix("");
    QVariantMap qvmConfiguration = pConfig->values(qstrPrefix, true);

    QFile qfExportFile(qstrParameter);
    if(qfExportFile.exists())
    {
        if(!qfExportFile.remove())
        {
            qCritical() << QString("Could not remove file \"%1\"").arg(qfExportFile.fileName());
            QCoreApplication::exit(EXIT_FAILURE);
            return;
        }
    }

    QSettings qsExport(qfExportFile.fileName(), QSettings::IniFormat);
    for(QVariantMap::Iterator qvmiConfiguration = qvmConfiguration.begin();
        qvmiConfiguration != qvmConfiguration.end();
        qvmiConfiguration++)
    {
        qsExport.setValue(qvmiConfiguration.key(), qvmiConfiguration.value());
    }
    QCoreApplication::exit(EXIT_SUCCESS);
}

void UConfiguration::importConfig(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis)
{
    QString qstrParameter = pThis->value(qstrlCommandLineOptionNames.first());
    if(qstrParameter.isEmpty())
        return showHelpOnError(QStringList(), pThis);

    QFile qfImportFile(pThis->value((qstrlCommandLineOptionNames.first())));
    if(!qfImportFile.exists())
    {
        qCritical() << QString("Could not find file \"%1\"").arg(qfImportFile.fileName());
        QCoreApplication::exit(EXIT_FAILURE);
    }

    QSettings qsImport(qfImportFile.fileName(), QSettings::IniFormat);

    UConfiguration* pConfig;
    if((pConfig = dynamic_cast<UConfiguration*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UConfiguration";
        QCoreApplication::exit(EXIT_FAILURE);
    }

    //  TODO:   Use correct prefix for configuration
    QString qstrPrefix("");
    pConfig->setValues(qsImport, qstrPrefix);
}

const QString UConfiguration::versionString()
{
    return QString(PROP_PRODUCTNAME + QLatin1Char(' ') +
                   GIT_VERSION + QLatin1Char('\n'));
}

void UConfiguration::showAllValues(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis, bool bRecursive)
{
    Q_UNUSED(qstrlCommandLineOptionNames);

    UConfiguration* pConfig;
    if((pConfig = dynamic_cast<UConfiguration*>(pThis)) == nullptr)
    {
        qCritical() << "Could not cast pointer to UConfiguration";
        QCoreApplication::exit(EXIT_FAILURE);
    }

    //  TODO:   Use correct prefix for configuration
    QVariantMap qvmConfiguration = pConfig->values("", bRecursive);

    const QString qstrParameter("Parameter");
    int iLongestParameterName = qstrParameter.size();
    foreach (QString qstrParameterName, qvmConfiguration.keys())
        if(qstrParameterName.size() > iLongestParameterName)
            iLongestParameterName = qstrParameterName.size();

    QTextStream qtsStdOut(pThis->outputStream(stdout));
    qtsStdOut << QString("%1%2 :  %3\n").arg(qstrParameter).arg("", iLongestParameterName - qstrParameter.size()).arg("Value");
    for(QVariantMap::Iterator qvmiConfiguration = qvmConfiguration.begin();
        qvmiConfiguration != qvmConfiguration.end();
        qvmiConfiguration++)
    {
        qtsStdOut << QString("%1%2 :  %3\n").arg(qvmiConfiguration.key()).arg("", iLongestParameterName - qvmiConfiguration.key().size()).arg(qvmiConfiguration.value().toString());
    }
    qtsStdOut.flush();
    QCoreApplication::exit(EXIT_SUCCESS);
}

void UConfiguration::setValues(const QStringList& qstrlKeys, const QVariantList& qvlValues, const QString& qstrPrefix)
{
    if(qstrlKeys.isEmpty())
    {
        qWarning() << "No settings to adjust.";
        return;
    }

    if(qstrlKeys.size() != qvlValues.size())
    {
        qWarning() << QString("Size of list of settings (%1) and values (%2) is not equal. Values might not be set correctly!")
                      .arg(qstrlKeys.size())
                      .arg(qvlValues.size());
    }

    beginGroup(qstrPrefix);
    for(int iEntry = 0; iEntry < qstrlKeys.size() && iEntry < qvlValues.size(); iEntry++)
        QSettings::setValue(qstrlKeys.at(iEntry), qvlValues.at(iEntry));
    endGroup();
}

void UConfiguration::setValues(const QVariantMap& qvmConfiguration, const QString& qstrPrefix)
{
    if(qvmConfiguration.isEmpty())
    {
        qWarning() << "No settings to adjust.";
        return;
    }

    beginGroup(qstrPrefix);
    for(QVariantMap::ConstIterator qvmiConfiguration = qvmConfiguration.begin();
        qvmiConfiguration != qvmConfiguration.end();
        qvmiConfiguration++)
    {
        QSettings::setValue(qvmiConfiguration.key(), qvmiConfiguration.value());
    }
    endGroup();
}

void UConfiguration::setValues(const QSettings& qsConfiguration, const QString& qstrPrefix)
{
    QStringList qstrlParameters = qsConfiguration.allKeys();

    if(qstrlParameters.isEmpty())
    {
        qWarning() << "No settings to adjust.";
        return;
    }

    beginGroup(qstrPrefix);
    foreach(QString qstrParameter, qstrlParameters)
        QSettings::setValue(qstrParameter, qsConfiguration.value(qstrParameter));
    endGroup();
}

QVariantMap UConfiguration::values(QStringList& qstrlKeys, QVariantList& qvlValues, const QString& qstrPrefix, const bool& bRecursive)
{
    beginGroup(qstrPrefix);

    if(qstrlKeys.isEmpty())
        qstrlKeys = bRecursive ? allKeys() : childKeys();

    QVariant qvValue;
    QVariantMap qvmRet;
    for(int iKey = 0; iKey < qstrlKeys.size(); iKey++)
    {
        qvValue = QSettings::value(qstrlKeys.at(iKey));
        if(qvValue.isNull() && iKey < qvlValues.size())
            QSettings::setValue(qstrlKeys.at(iKey), qvlValues.at(qstrlKeys.indexOf(qstrlKeys.at(iKey))));
        qvmRet.insert(qstrlKeys.at(iKey), qvValue);
    }
    qstrlKeys = qvmRet.keys();
    qvlValues = qvmRet.values();

    endGroup();

    return qvmRet;
}

QVariantMap UConfiguration::values(const QString& qstrPrefix, const bool& bRecursive)
{
    QStringList qstrlTemp;
    QVariantList qvlTemp;
    return values(qstrlTemp, qvlTemp, qstrPrefix, bRecursive);
}

void UConfiguration::valueChanged(QString qstrValueName)
{
    QVariant qvValue = parent()->property(qstrValueName.toUtf8());

    beginGroup(parent()->objectName());
    QSettings::setValue(qstrValueName, qvValue);
    endGroup();
}
