﻿#ifndef UCONFIGURATION_H
#define UCONFIGURATION_H

#include <QSettings>
#include <QPointer>
#include <utility/abstracts/uAbstractCommandLineParser.h>

namespace base {

namespace internal {

class UConfiguration : public QSettings , public UAbstractCommandLineParser
{
    Q_OBJECT
public:
    explicit UConfiguration(QObject* parent);
    void registerPropertyObject(const QString& qstrProperty, QPointer<QObject> qpPropertyObject);

    //  UAbstractCommandLineParser
    static void showVersion(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void showAllValues(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void showAllValuesRecursive(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void getValue(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void setValue(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void setDefaultValue(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void exportConfig(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);
    static void importConfig(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis);

    const QString versionString() override;

private:
    //  UAbstractCommandLineParser
    static void showAllValues(QStringList qstrlCommandLineOptionNames, UAbstractCommandLineParser* pThis, bool bRecursive);

    const QString objectName() override { return QSettings::objectName(); }
    static QMap<QString, QPointer<QObject>> s_qmPropertyObjects;

public slots:
    void setValues(const QStringList &qstrlKeys, const QVariantList& qvlValues, const QString &qstrPrefix);
    void setValues(const QVariantMap &qvmConfiguration, const QString &qstrPrefix);
    void setValues(const QSettings &qsConfiguration, const QString &qstrPrefix);
    QVariantMap values(QStringList &qstrlKeys, QVariantList &qvlValues, const QString &qstrPrefix = "", const bool& bRecursive = false);
    QVariantMap values(const QString &qstrPrefix = "", const bool& bRecursive = false);

    void valueChanged(QString qstrValueName);
};

} // namespace internal

} // namespace base

#endif // UCONFIGURATION_H
