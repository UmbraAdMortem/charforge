﻿# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Planed]
### Added
- Unit tests (CharForge)
- DataBase connection(SQLite)
- Menu - library
- GUI - library
- CLI - library
- QML-library
- OPC-UA - library
- TCPIP - library
- WebSocket - library
- UserManagement - library
- TCPIP - library
- Added command line "<module> --version"
- Added test case tc_uLog to UT-UFrameWorkBase
- Added test case tc_uConfiguration to UT-UFrameWorkBase

### Changed
- Splitting project into multiple repositories
- StartUp application and custom content to library

- Simplify command line(modules, "-" options, "--" commands), only help as option and command

### Deprecated

### Removed

### Fixed

### Security

## [InDevelopment]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [Unreleased]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.0.2] -2020-08-28
### Added
- Added command line "config --version"
- Added command line "--version-all"
- Added ULog::deleteInstance() to correctly destruct the singelton
- Added unit tests (UFrameWorkBase)
  - Added initTestCase()
  - Added cleanup()
  - Added cleaupTestCase()
  - Added test case tc_CLInput
  - Added data for tc_CLInput
    * -z; -zyx; --zyx
    * -h; -help; --help
    * -v; -version; --version
    * --version-all
    * config -h; config -help; config --help
    * config -v; config -version; config --version
- Added additional information to command line output on incorrect command line
  input

### Changed
- Splitting into custom content and UFrameWorkBase - library
- Changed command line output destination to be variable for unit tests
  dependent on if project starts with "UT-"
- Changed exit routine from "qt_call_post_routines(); ::exit(EXIT_CODE)" to
  "QCoreApplication::exit(EXIT_CODE)" and moved processCommandLineArguments to
  after QCoreApplication::exec();

### Deprecated

### Removed

### Fixed
- gitInfo.pl always printed "-dirty"
- Set properties correctly via CLO ("config -s")
- UT_UFrameWorkBase::cleanup(): spite currentDataTag by "test" only if "config -e test"

### Security

## [0.0.1] - 2020-05-30
### Added
- Command line argument handling
- Added command line version output
- Added command line help output
- Combined QPROPERTY-System with QSettings within UConfiguration
- Added command line "config --show-all"
- Added command line "config --show-recursive"
- Added command line "config --get-value <NAME>"
- Added command line "config --set-value <NAME>=<VALUE>"
- Added command line "config --set-default-value <NAME>"
- Added command line "config --export <FILEPATH>"
- Added command line "config --import <FILEPATH>"
- Added general property "langueage"
- Added logging
- Added logging property "logDir"
- Added logging property "logLevel"
- Added logging property "logDetail"

### Changed
- Remamed "settings" to "config"
- Adjusted help output to added command line options

### Fixed
- UConfiguration: index out of bounds bug

## [0.0.0] - 2020-03-22
### Added
- Settings handling
- Log handling
- Translation files of de-AT and en-GB
- Semantic versioning
- Changelog
