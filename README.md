# CharForge

Application to create character sheet for different pen and paper games.

All versions of 0.x.y.z are unreleased and their compatibility might be broken at any point.
All versions of w of w.x.y.z are are back- and forward compatible.
Any new version of x of w.x.y.z contains new features while being back- and forward compatible.
Any new version of y of w.x.y.z contains bug fixes to existing features while being back- and forward compatible.
All versions of z not being 0 contain commits of unfinished features or bug fixes and might not yet be back- and forward compatible.

# UFrameWork
## Class categories
### Worker

Workers are classes which are instanced once if they are needed, and are continuously available by a static instance() function.
They usually do not have a human interface of their own.
They might have general settings which can be accessed by the UConfiguration class.
(They are usually derived from UabstractsWorker.)

### Module

Modules are classes which are instanced if they are needed, and destroyed afterwards.
They have a humen interface of their own, which can be:
- command line options            (contained by UFrameWork)
- command line user interface     (contained by UFrameWorkCLUI)
- graphical widget user interface (contained by UFrameWorkGWUI)
- graphical qml user interface    (contained by UFrameWorkGQUI)
- detached user interface         (contained by UFrameWorkDUI)  (might be further split)
They have general settings and user specific settings which can be accessed by the USetings class.
They are derived from UabstractsModule.

### Utility

Utilities are classes which are instanced if they are needed, and destroyed afterwards.
They usually do not have a human interface of their own.
They might have general settings which can be accessed by the USettings class.
They are derived from UabstractsUtility.

## Workers
### UabstractsMainWorker
#### Configuration options / Properties
- Q_PROPERTY(QString language READ language WRITE setLanguage RESET resetLanguage NOTIFY languageChanged USER true FINAL)

### ULog
#### Configuration options / Properties
- Q_PROPERTY(QString logDir READ logDir WRITE setLogDir RESET resetLogDir NOTIFY logDirChanged USER true FINAL)
- Q_PROPERTY(ULogLevels logLevel READ logLevel WRITE setLogLevel RESET resetLogLevel NOTIFY logLevelChanged USER true FINAL)
- Q_PROPERTY(ULogDetails logDetail READ logDetail WRITE setLogDetail RESET resetLogDetail NOTIFY logDetailChanged USER true FINAL)

## Utilities
### UAbstractCommandLineParser
#### Command line options
- -v / --version
- -h / --help

### UConfiguration
#### Command line options
- config --show-all
- config --show-recursive
- config -- get-value <NAME>
- config --set-value <NAME>=<VALUE>
- config --set-default-value <NAME>
- config --export <FILEPATH>
- config --import <FILEPATH>
